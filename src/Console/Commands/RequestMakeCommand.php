<?php

namespace Einutech\Lumen\Toolkit\Console\Commands;

use Illuminate\Console\GeneratorCommand;

class RequestMakeCommand extends GeneratorCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'einu:make:request';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new FormRequest class';
    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'FormRequest';
    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return __DIR__.'/../Stubs/request.stub';
    }
    /**
     * Get the default namespace for the class.
     *
     * @param  string  $rootNamespace
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace.'\Http\Requests';
    }
}