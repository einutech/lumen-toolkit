<?php

namespace Einutech\Lumen\Toolkit;

use Einutech\Lumen\Toolkit\Http\FormRequest;
use Laravel\Lumen\Http\Redirector;
use Symfony\Component\HttpFoundation\Request;
use Illuminate\Contracts\Validation\ValidatesWhenResolved;
use Illuminate\Support\ServiceProvider;

class LumenToolkitServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->afterResolving(ValidatesWhenResolved::class, function ($resolved) {
            $resolved->validate();
        });

        $this->app->resolving(FormRequest::class, function ($request, $app) {
            $this->initializeRequest($request, $app['request']);
            $request->setContainer($app)->setRedirector($app->make(Redirector::class));
        });
    }

    public function register()
    {
        $this->registerControllerMakeCommand();
        $this->registerModelMakeCommand();
        $this->registerRequestMakeCommand();
    }

    protected function initializeRequest(FormRequest $form, Request $current)
    {
        $files = $current->files->all();
        $files = is_array($files) ? array_filter($files) : $files;

        $form->initialize(
            $current->query->all(), $current->request->all(), $current->attributes->all(),
            $current->cookies->all(), $files, $current->server->all(), $current->getContent()
        );

        $form->setJson($current->json());

        if ($session = $current->getSession()) {
            $form->setLaravelSession($session);
        }

        $form->setUserResolver($current->getUserResolver());
        $form->setRouteResolver($current->getRouteResolver());
    }

    protected function registerControllerMakeCommand()
    {
        $this->app->singleton('command.einu.controller.make', function($app){
            return $app['Einutech\Lumen\Toolkit\Console\Commands\ControllerMakeCommand'];
        });

        $this->commands('command.einu.controller.make');
    }

    protected function registerModelMakeCommand()
    {
        $this->app->singleton('command.einu.model.make', function($app){
            return $app['Einutech\Lumen\Toolkit\Console\Commands\ModelMakeCommand'];
        });

        $this->commands('command.einu.model.make');
    }

    protected function registerRequestMakeCommand()
    {
        $this->app->singleton('command.einu.request.make', function($app){
            return $app['Einutech\Lumen\Toolkit\Console\Commands\RequestMakeCommand'];
        });

        $this->commands('command.einu.request.make');
    }
}